// ## Завдання
// 1. Створити клас Employee, у якому будуть такі характеристики - name (ім'я), age (вік), salary (зарплата). Зробіть так, щоб ці характеристики заповнювалися під час створення об'єкта.
// 2. Створіть гетери та сеттери для цих властивостей.
// 3. Створіть клас Programmer, який успадковуватиметься від класу Employee, і який матиме властивість lang (список мов).
// 4. Для класу Programmer перезапишіть гетер для властивості salary. Нехай він повертає властивість salary, помножену на 3.
// 5. Створіть кілька екземплярів об'єкта Programmer, виведіть їх у консоль.

class Employee {
    constructor(name, age, salary) {
        this._name = name;
        this._age = age;
        this._salary = salary;
    }
    get age() {
        return this._age
    }
    set age(value) {
        this._age = value
    }
    get name() {
        return this._name
    }
    set name(value) {
        this._name = value
    }
}
class Programmer extends Employee {
    constructor(name, age, salary, lang) {
        super(name, age, salary);
        this._lang = lang;
        this._salary = salary
    }

    get salary() {
        return this._salary *3
    }
    set salary(value) {
        this._salary = value
    }
}
const ivan = new Programmer('Ivan', '20', '100', 'fr')
const petro = new Programmer('petro', '23', '500', 'eng')
const sidar = new Programmer('sidar', '25', '300', 'uk')

console.log(ivan)
console.log(petro)
console.log(sidar)

